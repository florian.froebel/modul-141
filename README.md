# Modul-141


# SQL und seine Untersprachen

SQL (Structured Query Language) ist eine standardisierte Programmiersprache, die für die Verwaltung und Manipulation von Daten in relationalen Datenbanksystemen verwendet wird. SQL besteht aus mehreren Untersprachen, die jeweils spezifische Aufgaben erfüllen:

## DDL (Data Definition Language)

**Aufgabe**: Definiert die Struktur der Datenbanken, Tabellen, Sichten, Indizes und andere Datenbankobjekte.

**Wichtige Befehle**:
- `CREATE`: Erstellen neuer Datenbankobjekte (z.B. Tabellen, Datenbanken).
- `ALTER`: Ändern bestehender Datenbankobjekte.
- `DROP`: Löschen von Datenbankobjekten.
- `TRUNCATE`: Entfernen aller Datensätze aus einer Tabelle, ohne deren Struktur zu ändern.

## DML (Data Manipulation Language)

**Aufgabe**: Bearbeitet und verwaltet die Daten innerhalb der Datenbankobjekte.

**Wichtige Befehle**:
- `SELECT`: Abfragen von Daten aus einer oder mehreren Tabellen.
- `INSERT`: Einfügen neuer Datensätze in eine Tabelle.
- `UPDATE`: Ändern bestehender Datensätze in einer Tabelle.
- `DELETE`: Löschen von Datensätzen aus einer Tabelle.

## DCL (Data Control Language)

**Aufgabe**: Steuert die Zugriffsrechte und Berechtigungen innerhalb der Datenbank.

**Wichtige Befehle**:
- `GRANT`: Gewähren von Benutzerberechtigungen.
- `REVOKE`: Entziehen von Benutzerberechtigungen.

## Zusammenfassung

- **DDL**: Definiert und verändert die Struktur der Datenbank (z.B. `CREATE`, `ALTER`, `DROP`).
- **DML**: Manipuliert die Daten in der Datenbank (z.B. `SELECT`, `INSERT`, `UPDATE`, `DELETE`).
- **DCL**: Kontrolliert den Zugriff auf die Daten und ihre Sicherheit (z.B. `GRANT`, `REVOKE`).

Diese Untersprachen von SQL arbeiten zusammen, um eine umfassende Verwaltung und Manipulation von Daten in relationalen Datenbanksystemen zu ermöglichen.


# M141 - DB-Systeme in Betrieb nehmen

## Repetition zu SQL - M164

Tragen Sie für jede der unten aufgeführten Tätigkeiten den passenden Befehl ein. Geben Sie rechts die entsprechende Befehlsgruppe an (DDL, DML, DCL). Markieren Sie in der letzten Spalte die "gefährlichen" Befehle.

1. DDL
```
SELECT * FROM <table>;
```
2. DCL
```
USE DATABASE <name>;
```
3. DDL
```
CREATE DATABASE [NAME]; 
```
4. DDL
```
CREATE TABLE [TABLE NAME];
```
5. DDL
```
DROP TABLE [TABLE NAME];
```
6. DDL
```
DESC [TABLE NAME];  
```
7. DDL
```
SHOW DATABASE;
```
8.  DDL
```
SHOW TABLES;
```
9. DML
```
INSERT INTO <table> (<column1>, <>) VALUES (<value>, );
```
10. DML
```
UPDATE <table>
SET <Column>
WHERE <condition>/<column>=<value>;
```
11. DML
```
DELETE FROM <table>
WHERE <column> = <value>;
```
12. DDL
```
DROP INDEX <indexname>;
```
13. DDL
```
CREATE INDEX <indexname> ON <tablename> <columnname>;
```
14. DDL
```
ALTER TABLE <tablename> DROP COLUMN <columnname>;
```

## Transaktionen
Mit Transaktionen können mehrere SQL-Anweisungen in einem Block gekapselt werden, um diesen Block dann "sicher" auszuführen. Sicher bedeutet hier „ganz oder gar nicht“. Transaktionen erhöhen in vielen Fällen die Sicherheit und Konsistenz der Daten, erleichtern die Programmierung und erhöhen eventuell auch die Effizienz von Anwendungen. Wir verwenden im Folgenden den Tabellentyp ``InnoDB.``

### BEGIN, COMMIT und ROLLBACK

Indem Sie eine Tabelle vom Typ BDB, InnoDB oder Gemini erzeugen, haben Sie noch nichts gewonnen. Damit Sie den Vorteil von Transaktionen nutzen können, müssen Sie diese mit `BEGIN` (oder `START TRANSACTION`) einleiten und mit `COMMIT;` beenden. `COMMIT;` führt alle seit `BEGIN` angegebenen SQL-Kommandos tatsächlich aus.

Obig beschriebenes Problem könnte also so realisiert werden:


```sql
SET @uebertrag_var = 1000;   -- Übertrag in User-Var speichern

BEGIN;           -- oder START TRANSACTION

  -- Überprüfung:
  SELECT IF(Saldo >= @uebertrag_var, @uebertrag_var, 0)  INTO  @uebertrag_var
    FROM   tbl_konto WHERE  name = 'Von';  -- Schauen ob Saldo genug hoch ist, sonst Übertrag nullen.

  -- Übertrag:
  UPDATE tbl_konto SET Saldo = Saldo - @uebertrag_var WHERE name = 'Von';  
  UPDATE tbl_konto SET Saldo = Saldo + @uebertrag_var WHERE name = 'Nach';
  
COMMIT; --- oder ROLLBACK;
```

Statt `COMMIT` auszuführen, können Sie die gesamte Transaktion aber auch mit `ROLLBACK` (manuell) widerrufen. `ROLLBACK` wird *automatisch ausgeführt*, falls es zu einem *Verbindungsabbruch* kommt.

## ACID

- Atomicity
- Consistency
- Isolation
- Durability


Die Abkürzung **ACID** steht für die notwendigen Eigenschaften, um Transaktionen auf Datenbankmanagementsystemen (DBMS) einsetzen zu können. Es steht für **A**tomarität (atomicity), **K**onsistenz (consistency), **I**soliertheit (isolation) und **D**auerhaftigkeit (durability). Im Deutschen spricht man gelegentlich auch von AKID.

### Atomarität

Ein Block von SQL-Anweisungen (eine oder mehrere) wird **entweder ganz oder gar nicht ausgeführt**: Das Datenbanksystem behandelt diese Anweisungen wie eine einzelne, unteilbare (daher atomare) Anweisungen. Gibt es bei einem Statement technische Probleme oder tritt ein definierter Zustand ein, der einen Abbruch erfordert (z. B. Kontoüberziehung), werden auch die bereits durchgeführten Operationen nicht auf der Datenbank wirksam.

### Konsistenz

Nach Abschluss der Transaktion **befindet sich die Datenbank in einem konsistenten Zustand**. Das bedeutet, dass Widerspruchsfreiheit der Daten gewährleistet sein muss. Das gilt für alle definierten Integritätsbedingungen genauso, wie für die Schlüssel- und Fremdschlüsselverknüpfungen.

### Isoliertheit

Die Ausführungen verschiedener Datenbankmanipulationen dürfen **sich nicht gegenseitig beeinflussen**. Mittels Sperrkonzepte, Arbeitskopien und Timestamps wird sichergestellt, dass durch eine Transaktion verwendete Daten nicht vor Beendigung dieser Transaktion verändert werden. Die Sperrungen müssen so kurz und begrenzt wie möglich gehalten werden, da sie die Performance der anderen Operationen beeinflusst.

### Dauerhaftigkeit

Nach Abschluss einer Transaktion müssen die Manipulationen **dauerhaft in der Datenbank gespeichert** sein. Der Pufferpool speichert häufig verwendete Teile einer Datenbank im Arbeitsspeicher. Diese müssen aber nach Abschluss auf der Festplatte gespeichert werden.

